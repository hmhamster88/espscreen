#include <NTPClient.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <FS.h>
#include <vector>
#include <ArduinoJson.h>
#include <Reflector.h>
#include <LiquidCrystal.h>
#include "src/Topic.h"
#include "src/Timer.h"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

void simplePrint(const String& text);

std::vector<Topic> topics;
int showTopicIndex = 0;

class TopicsWrapper {
  public:
  TopicsWrapper(std::vector<Topic>& topics): topics(topics) {}
  std::vector<Topic>& topics;
  template<class Reflector>
    void reflect(Reflector& reflector) {
        reflector
                (topics, "topics", std::vector<Topic>(), "Topics");
    }
};

class WiFiSettings{
public:
    String ssid;
    String password;

    template<class Reflector>
    void reflect(Reflector& reflector) {
        reflector
                (ssid, "ssid", String("ssid"), "SSID")
                (password, "password", String("password"), "Password");
    }
};

class Settings {
  const char *  fileName = "/config.json";
public:
  WiFiSettings wiFi;
  String mdnsName;
  int timeOffset;
  int showTopicTime = 3;
  

    template<class Reflector>
    void reflect(Reflector& reflector) {
        reflector
                (wiFi, "wiFi", WiFiSettings(), "WiFi")
                (mdnsName, "mdnsName", String("espscreen"), "mDNS name")
                (timeOffset, "timeOffset", 3, "Time Offset, hours")
                (showTopicTime, "showTopicTime", 3, "Show Topic Time, seconds");
    }

    void load () {
        File file = SPIFFS.open(fileName, "r");
        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(file);
        JsonDeserializer<JsonObject> deserializer(root);
        reflect(deserializer);
        file.close();
    }

    void save () {
      File file = SPIFFS.open(fileName, "w");
      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      JsonSerializer<JsonObject> serializer(root);
      reflect(serializer);
      root.printTo(file);
      file.close();
    }
};

Topic& getTopic(const String& topicName) {
  for(Topic& topic: topics) {
    if (topic.name == topicName) {
      return topic;
    }
  }
  Topic topic;
  topic.name = topicName;
  topics.push_back(topic);
  return topics[topics.size() - 1];
}

void updateTopic(const String& topicName, const String& value) {
  Topic& topic = getTopic(topicName);
  topic.value = value;
  topic.lastUpdateTime = timeClient.getEpochTime();
}

Settings settings;
Timer timer;

ESP8266WebServer server(80);
const int rs = D0, en = D1, d4 = D2, d5 = D3, d6 = D4, d7 = D5, buttonPin = D6;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
unsigned long lastTimeUpdate = 0;

void simplePrint(const String& text) {
  lcd.clear();
  int line = 1;
  for(int i = 0; i < text.length(); i++) {
    char c = text.charAt(i);
    if (c == '\n') {
      lcd.setCursor(0, line);
      line++;
    } else {
      lcd.print(c);
    }
  }
}

bool connectToSsid() {

  String text = "Connecting to\n" +
    settings.wiFi.ssid;

  WiFi.mode(WIFI_STA);
  WiFi.begin(settings.wiFi.ssid.c_str(), settings.wiFi.password.c_str());

  simplePrint(text);
  int counter = 10;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    text += ".";
    simplePrint(text);
    counter--;
    if(counter == 0) {
      WiFi.disconnect();
      return false;
    }
  }

  text = "Connected\n" + WiFi.localIP().toString();
  simplePrint(text);
  delay(1000);
  return true;
}

bool startAp() {
  simplePrint("Try to start ap\n" + settings.wiFi.ssid + "\npass:\n" + settings.wiFi.password);
  boolean result = WiFi.softAP((settings.wiFi.ssid + "_ap").c_str(), settings.wiFi.password.c_str());
  if(result == true)
  {
    IPAddress myIP = WiFi.softAPIP();
    simplePrint("AP IP address:\n" + myIP.toString());
    delay(1000);
    return true;
  }
  simplePrint(String("AP Failed!"));
  delay(1000);
  return false;
}

void getTopicsData() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonSerializer<JsonObject> serializer(root);
  TopicsWrapper wrapper(topics);
  wrapper.reflect(serializer);
  String response;
  root.printTo(response);
  server.send(200, "application/json", response);
}

void postTopicsData() {
  String plain = server.arg("plain");
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parse(plain);
  JsonDeserializer<JsonObject> deserializer(root);
  TopicsWrapper wrapper(topics);
  wrapper.reflect(deserializer);
  for(Topic& topic: wrapper.topics) {
    updateTopic(topic.name, topic.value);
  }
  server.send(200, "application/json", "{}");
}

void getSettingsData() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonMetaSerializer<JsonObject> serializer(root);
  settings.reflect(serializer);
  String response;
  root.printTo(response);
  server.send(200, "application/json", response);
}

unsigned long lastShowTime = 0;

void showNextTopic() {
  showTopicIndex++;
  if (showTopicIndex >= topics.size()) {
    showTopicIndex = 0;
  }
}

void applySettings() {
  timeClient.setTimeOffset(settings.timeOffset * 60 * 60);
  timer.set(showNextTopic, settings.showTopicTime * 1000);
}

void postSettingsData() {
  String plain = server.arg("plain");
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parse(plain);
  JsonMetaDeserializer<JsonObject> deserializer(root);
  settings.reflect(deserializer);
  applySettings();
  settings.save();
  getSettingsData();
}

String timePart(int part, String letter) {
  if (part == 0) {
    return "";
  }
  if (part < 10) {
    return "0" + String(part) + letter;
  }
  return String(part) + letter;
}

String formatTimeSpan(unsigned long span) {
  if (span == 0) {
    return "now";
  }
  int seconds = span % 60;
  span = span / 60;
  int minutes = span % 60;
  span = span / 60;
  int hours = span % 60;
  span = span / 60;
  int days = span % 24;

  return timePart(days, "d") + timePart(hours, "h") + timePart(minutes, "m") + timePart(seconds, "s");
}

void updateLcd() {
  if (topics.size() == 0) {
    return;
  }
  Topic& topic = topics[showTopicIndex];
  unsigned long now = timeClient.getEpochTime();
  unsigned long span = now - topic.lastUpdateTime;
  String text = topic.name + ": " + topic.value + "\n";
  if (span == 0) {
    text += "Now";
  } else {
    text += formatTimeSpan(span) + " Ago";
  }
  simplePrint(text);
}

void sendTime() {
  updateTopic("Time", timeClient.getFormattedTime());
}

bool lastButtonState = true;

void buttonHandler() {
  bool newState = digitalRead(buttonPin);
  if (!newState && newState != lastButtonState) {
    timer.reset(showNextTopic);
    showNextTopic();
    updateLcd();
  }
  lastButtonState = newState;
}

void postRestart() {
  simplePrint("Restarting...");
  delay(1000);
  ESP.restart();
}

void setup() {
  pinMode(buttonPin, INPUT_PULLUP);
  SPIFFS.begin();
  Serial.begin(115200);
  settings.load();
  lcd.begin(16, 2);

  if(!connectToSsid()) {
    startAp();
  }

  
  if (!MDNS.begin(settings.mdnsName.c_str())) {
    simplePrint("MDNS name:\n" + settings.mdnsName);
    delay(1000);
  }

  timeClient.begin();

  server.on("/settings_data", HTTP_POST, postSettingsData);
  server.on("/settings_data", HTTP_GET, getSettingsData);
  server.on("/topics.json", HTTP_GET, getTopicsData);
  server.on("/topics.json", HTTP_POST, postTopicsData);
  server.on("/restart", HTTP_POST, postRestart);

  server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/index.js", SPIFFS, "/index.js");
  server.begin();

  timer.set(sendTime, 1000);
  timer.set(updateLcd, 1000);
  timer.set(buttonHandler, 100);

  applySettings();
}

void loop() {
  server.handleClient();
  unsigned long currMillis = millis();
  timer.update();
  timeClient.update();
}
