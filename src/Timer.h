#ifndef TIMER_H
#define TIMER_H

#include <vector>

class TimerRecord {
public:
    int period;
    unsigned long lastUpdateTime = 0;
    void (*handler)();
};

class Timer {
    std::vector<TimerRecord> records;
public:
    Timer() {
    }

    TimerRecord& getRecord(void (*handler)()) {
        for(TimerRecord& record: records) {
            if (record.handler == handler) {
                return record;
            }
        }
        TimerRecord record;
        record.handler = handler;
        records.push_back(record);
        return records[records.size() - 1];
    }

    void set(void (*handler)(), int period) {
        TimerRecord& record = getRecord(handler);
        record.period = period;
    }

    void reset(void (*handler)()) {
        TimerRecord& record = getRecord(handler);
        record.lastUpdateTime = millis();
    }

    void update() {
        unsigned long currMillis = millis();
        for(TimerRecord& record: records) {
            if (currMillis - record.lastUpdateTime > record.period) {
                record.handler();
                record.lastUpdateTime = currMillis;
            }
        }
    }

};

#endif
