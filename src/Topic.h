#ifndef TOPIC_H
#define TOPIC_H

class Topic {
public:
	String name;
	String value;
	int lastUpdateTime = 0;

	template<class Reflector>
	void reflect(Reflector& reflector) {
		reflector
		(name, "name", String("name"), "Name")
			(value, "value", String("value"), "Value")
			(lastUpdateTime, "lastUpdateTime", 0, "Last Update Time");
	}
};

#endif // !TOPIC_H
